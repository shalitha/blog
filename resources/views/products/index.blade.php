@extends('master')

@section('content')

    <h3>Product catalogue</h3>
    <table class="table table-striped">
      <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Price</th>
        <th>&nbsp;</th>
      </tr>
      @foreach($products as $product)
      <tr>
        <td>{{ $product->name }}</td>
        <td>{{ $product->category }}</td>
        <td>Rs {{ $product->price }}</td>
        <td>
            <a href="{{ url('/products/'.$product->id.'/edit') }}" class="btn btn-default">Edit</a>
            <a href="" class="btn btn-warning">Delete</a>
        </td>
      </tr>
      @endforeach
    </table>
@stop