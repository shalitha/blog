@extends('master')

@section('content')
    <h3>Edit Product</h3>
    <form action="{{ url('/products/'.$product->id.'/update') }}" method="POST">
        <div class="form-group">
            <label class="control-label">Name</label>
            <input class="form-control" type="text" name="name" value="{{ $product->name }}"/>
        </div>
        <div class="form-group">
            <label class="control-label">Price</label>
            <input class="form-control" type="text" name="price" value="{{ $product->price }}"/>
        </div>
        <div class="form-group">
            <label class="control-label">Category</label>
            <input class="form-control" type="text" name="category" value="{{ $product->category }}"/>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Save"/>
        </div>
    </form>
@stop