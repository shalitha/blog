<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Schema;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Http\Request;

class PagesController extends Controller {

	public function home(){
        $products = Category::find(1)->products;
        dd($products);
        $data = array(
            'title' => 'My first laravel app',
            'heading' => 'Hi',
            'para' => 'Welcome to my site!',
            'list' => array('Jan', 'Feb', 'Mar', "Apr")
        );
        return view('home')->with($data);
    }

    public function about(){
        $data = array(
            'title' => 'My first laravel app',
            'heading' => 'Hi'
        );
        return view('pages/about')->with($data);
    }

}
