<?php

Route::get('/', 'PagesController@home');

Route::get('/about-us', 'PagesController@about');

//to view all products
Route::get('/products', 'ProductController@index');

//to edit a product with an id
Route::get('/products/{id}/edit', 'ProductController@edit');

//to save the edited product
Route::post('/products/{id}/update', 'ProductController@update');